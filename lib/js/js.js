$(function(){

    var image = [
        'https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-706558.jpg',
        'https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-700301.png',
        'https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-715144.jpg'
    ]
    var dots = $('#dots');
    var ref = 0;
    var n = 1;
// récupération des items du local storage et affichage des images après chargement de la page
    $('document').ready(function(){
        if (localStorage.getItem("articles")) {
        }
        for (var i = 0; i < image.length; i++) {
            $('#slider').html('<img class="img-slider" id="img-slider" src="'+image[i]+'">');

        }
    });
//parcours le tableau en ajoutant +1 ou -1 à un indice
    function plusSlide(n){
        var interval = setInterval(function(){
            showSlide(ref+=n);
        }, 4000)
        $('.right-btn').click(function(){
            showSlide(ref+=n)
            console.log(ref)
            clearInterval(interval)
        });
        $('.left-btn').click(function(){
            showSlide(ref-=n)
            console.log(ref)
            clearInterval(interval)
        });
    }
    function currentSlide(n) {
        showSlides(ref = n);
    }

    $('#dots').click(function(){
        currentSlide(image[i])
    })
//Bouclage du slider et smooth transition
    function showSlide(n){
        var indexImg = image.length;
        $('#img-slider').fadeOut(500, function() {
            $('#img-slider').attr("src",image[n-1]).fadeIn(500);
            if (n >= indexImg) {
                ref = 0;
            }
            if (n <= 0) {
                if (n == 0) {
                    $('#img-slider').attr("src",image[indexImg-1])
                }
                ref = indexImg
            }
        });
    }
// ajout de slide au local storage
//simple de reprise du code de M.Leygues (votre code) et adaptation à la situation
    $('#sub').click(function() {
            var Article = {};
            Article.img_src  = $('#link').val();
            addArticleLocal(Article);
    });

    function addArticleLocal(Article) {
    	if(typeof localStorage!='undefined') {
    		var articles_json,
    			articles = [];

    		if (localStorage.getItem("articles")) {
    			articles_json = localStorage.getItem("articles");
    			articles = JSON.parse(articles_json);
    		}

    		articles.push(Article);
    		articles_json = JSON.stringify(articles);
    		localStorage.setItem("articles",articles_json);
    		addArticleHTML(Article);

    	} else {
    		alert("localStorage n'est pas supporté");
    	}
    }
// ajout de l'image au tableu 'image'
    function addArticleHTML(Article) {
    	image.push(Article.img_src);
    }

    plusSlide(n);
    showSlide(ref);
    displayArticles()
//récupération des images du local storage
    function displayArticles() {
    	// Détection
    	if(typeof localStorage!='undefined' || localStorage.getItem("articles")) {
    		var articles = [];
    		if (localStorage.getItem("articles")) {

    			var articles_json = localStorage.getItem("articles"),
    				articles      = JSON.parse(articles_json);
    		}

    		if (articles.length > 0) {
    			$('#slider img').remove();

    			var article  = ""

    			for (var i = 0, c = articles.length; i < c; i++) {
    				addArticleHTML(articles[i]);
    			}
    		}

    	} else {
    	  alert("localStorage n'est pas supporté");
    	}
    }
});
